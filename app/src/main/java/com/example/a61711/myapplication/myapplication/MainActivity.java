package com.example.a61711.myapplication.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a61711.myapplication.R;
import com.example.a61711.myapplication.data.Music;
import com.example.a61711.myapplication.data.MusicList;
import com.example.a61711.myapplication.model.PropertyBean;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {

    private ImageButton imgBtn_Previous;
    private ImageButton imgBtn_PlayOrPause;
    private ImageButton imgBtn_Stop;
    private ImageButton imgBtn_Next;
    private ListView list;
    //歌曲列表对象
    private ArrayList<Music> musicArrayList;

    private int number = 0;
    // 播放状态
    private int status;
    // 广播接收器
    private StatusChangedReceiver receiver;
    private RelativeLayout root_Layout;
    private TextView text_Current;
    private TextView text_Duration;
    private SeekBar seekBar;
    private TextView textView;
    private Handler seekBarHandler;
    //当前歌曲信息
    private int duration;
    private int time;

    //进度条控制常量
    private static final int PROGRESS_INCREASE = 0;
    private static final int PROGRESS_PAUSE = 1;
    private static final int PROGRESS_RESET = 2;

    private MediaPlayer player = new MediaPlayer();


    //播放模式常量选择
    private static final int MODE_LIST_SEQUENCE = 0;
    private static final int MODE_SINGLE_CYCLE = 1;
    private static final int MODE_LIST_CYCLE = 2;
    private static final int MODE_RANDOM = 3;
    private int playmode;

    private static boolean isExit = false;//退出标记

    //音量
    private TextView tv_vol;
    private SeekBar seekBar_vol;
    private ImageView volume_t;

    //睡眠时间组件
    private ImageView iv_sleep;
    private Timer timer_sleep;
    private static final boolean NOTSLEEP = false;
    private static final boolean ISSLEEP = true;
    private int sleepminute = 20;
    private static boolean sleepmode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        duration=0;
        time=0;
        findViews();
        registerListeners();
        initMusicList();
        initListView();
        checkMusicfile();
        // 绑定广播接收器，可以接收广播
        bindStatusChangedReceiver();
        initSeekBarHandler();
        startService(new Intent(this, MusicService.class));
        status = MusicService.COMMAND_STOP;
        //默认播放模式为顺序
        playmode = MainActivity.MODE_LIST_SEQUENCE;
        //默认睡眠状态关闭
        sleepmode = MainActivity.NOTSLEEP;
        textView.setSelected(true);
    }

    /** 绑定广播接收器 */
    private void bindStatusChangedReceiver() {
        receiver = new StatusChangedReceiver();
        IntentFilter filter = new IntentFilter(
                MusicService.BROADCAST_MUSICSERVICE_UPDATE_STATUS);
        registerReceiver(receiver, filter);
    }

    private void findViews() {
        imgBtn_Previous = (ImageButton) findViewById(R.id.imageButton1);
        imgBtn_PlayOrPause = (ImageButton) findViewById(R.id.imageButton2);
        imgBtn_Stop = (ImageButton) findViewById(R.id.imageButton3);
        imgBtn_Next = (ImageButton) findViewById(R.id.imageButton4);
        list = (ListView) findViewById(R.id.main_listview);
        textView = (TextView)findViewById(R.id.textView);
        seekBar = (SeekBar)findViewById(R.id.seekBar1);
        text_Current = (TextView)findViewById(R.id.textView1);
        text_Duration = (TextView)findViewById(R.id.textView2);
        root_Layout = (RelativeLayout)findViewById(R.id.relativeLayout1);
        tv_vol = (TextView)findViewById(R.id.volumeText);
        seekBar_vol = (SeekBar)findViewById(R.id.volumeBar);
        iv_sleep = (ImageView)findViewById(R.id.main_iv_sleep);
        volume_t = (ImageView)findViewById(R.id.main_iv_audioManager);
    }

    //组件监听响应
    private void registerListeners() {
        imgBtn_Previous.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                //sendBroadcastOnCommand(MusicService.COMMAND_PREVIOUS);
                switch (playmode){
                    case MainActivity.MODE_LIST_CYCLE:
                        if(number == 0){
                            number = MusicList.getMusicList().size()-1;
                            sendBroadcastOnCommand(MusicService.COMMAND_PLAY);
                        }
                        else {
                            sendBroadcastOnCommand(MusicService.COMMAND_PREVIOUS);
                        }
                        break;
                    case MainActivity.MODE_RANDOM:
                        sendBroadcastOnCommand(MusicService.COMMAND_RANDOM_NEXT);
                        break;
                    default:
                        sendBroadcastOnCommand(MusicService.COMMAND_PREVIOUS);
                }
            }
        });
        imgBtn_PlayOrPause.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                switch (status) {
                    case MusicService.STATUS_PLAYING:
                        sendBroadcastOnCommand(MusicService.COMMAND_PAUSE);
                        break;
                    case MusicService.STATUS_PAUSED:
                        sendBroadcastOnCommand(MusicService.COMMAND_RESUME);
                        break;
                    case MusicService.COMMAND_STOP:
                        sendBroadcastOnCommand(MusicService.COMMAND_PLAY);
                    default:
                        break;
                }
            }
        });
        imgBtn_Stop.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                sendBroadcastOnCommand(MusicService.COMMAND_STOP);
            }
        });
        imgBtn_Next.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                switch (playmode){
                    case MainActivity.MODE_LIST_CYCLE:
                        if ((number) == MusicList.getMusicList().size()-1) {
                            number = 0;
                            sendBroadcastOnCommand(MusicService.COMMAND_PLAY);
                        }
                        else
                            sendBroadcastOnCommand(MusicService.COMMAND_NEXT);
                        break;
                    case MainActivity.MODE_RANDOM:
                        sendBroadcastOnCommand(MusicService.COMMAND_RANDOM_NEXT);
                        break;
                    default:
                        sendBroadcastOnCommand(MusicService.COMMAND_NEXT);
                }
            }
        });
        list.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                number = position;
                sendBroadcastOnCommand(MusicService.COMMAND_PLAY);
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(status != MusicService.STATUS_STOPPED){
                    time = seekBar.getProgress();
                    text_Current.setText(formatTime(time));
                    //发送广播给Musicservice,执行跳转
                    sendBroadcastOnCommand(MusicService.COMMAND_SEEK_TO);
                }
                if(status == MusicService.STATUS_PLAYING){
                    //发送广播给MusicService，执行跳转
                    sendBroadcastOnCommand(MusicService.COMMAND_SEEK_TO);
                    //进度条恢复移动
                    seekBarHandler.sendEmptyMessageDelayed(PROGRESS_INCREASE,1000);
                }
                if(status == MusicService.STATUS_PAUSED){
                    sendBroadcastOnCommand(MusicService.COMMAND_SEEK_TO);
                    sendBroadcastOnCommand(MusicService.COMMAND_RESUME);
                    seekBarHandler.sendEmptyMessageDelayed(PROGRESS_INCREASE,1000);
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                //进度条暂停移动
                seekBarHandler.sendEmptyMessage(PROGRESS_PAUSE);
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }
        });
    }


    //遍历本地数据库，获取歌曲信息
    private void initMusicList() {
        musicArrayList = MusicList.getMusicList();
        if(musicArrayList.isEmpty())
        {
            Cursor mMusicCursor = this.getContentResolver().query(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    null, null, null,
                    MediaStore.Audio.AudioColumns.TITLE);
            //标题
            int indexTitle = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE);
            //艺术家
            int indexArtist = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST);
            //总时长
            int indexTotalTime = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION);
            //路径
            int indexPath = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);

            /**通过mMusicCursor游标遍历数据库，并将Music类对象加载带ArrayList中*/
            for (mMusicCursor.moveToFirst(); !mMusicCursor.isAfterLast(); mMusicCursor
                    .moveToNext()) {
                String strTitle = mMusicCursor.getString(indexTitle);
                String strArtist = mMusicCursor.getString(indexArtist);
                String strTotoalTime = mMusicCursor.getString(indexTotalTime);
                String strPath = mMusicCursor.getString(indexPath);

                if (strArtist.equals("<unknown>"))
                    strArtist = "无艺术家";
                Music music = new Music(strTitle, strArtist, strPath, strTotoalTime);
                musicArrayList.add(music);
            }
        }
    }

    private void initListView() {
        List<Map<String, String>> list_map = new ArrayList<Map<String, String>>();
        HashMap<String, String> map;
        SimpleAdapter simpleAdapter;
        for (Music music : musicArrayList) {
            map = new HashMap<String, String>();
            map.put("musicName", music.getmusicName());
            map.put("musicArtist", music.getmusicArtist());
            list_map.add(map);
        }

        String[] from = new String[] { "musicName", "musicArtist" };
        int[] to = { R.id.listview_tv_title_item, R.id.listview_tv_artist_item };

        simpleAdapter = new SimpleAdapter(this, list_map, R.layout.listview,from, to);
        list.setAdapter(simpleAdapter);
    }


    private void checkMusicfile()
    {
        if (musicArrayList.isEmpty()) {
            imgBtn_Next.setEnabled(false);
            imgBtn_PlayOrPause.setEnabled(false);
            imgBtn_Previous.setEnabled(false);
            imgBtn_Stop.setEnabled(false);
            Toast.makeText(getApplicationContext(), "当前没有歌曲文件",Toast.LENGTH_SHORT).show();
        } else {
            imgBtn_Next.setEnabled(true);
            imgBtn_PlayOrPause.setEnabled(true);
            imgBtn_Previous.setEnabled(true);
            imgBtn_Stop.setEnabled(true);
        }
    }


    protected void onResume() {//再次进入是实时更新信息
        super.onResume();
        sendBroadcastOnCommand(MusicService.COMMAND_CHECK_IS_PLAYING);
        PropertyBean propertty = new PropertyBean(MainActivity.this);
        String theme = propertty.getTheme();
        setTheme(theme);
        audio_Control();
        if(sleepmode == MainActivity.NOTSLEEP)iv_sleep.setVisibility(View.INVISIBLE);
        else iv_sleep.setVisibility(View.VISIBLE);
    }

    private String formatTime(int msec) {
        int minute = msec / 1000 / 60;
        int second = msec / 1000 % 60;
        String minuteString;
        String secondString;
        if (minute < 10) {
            minuteString = "0" + minute;
        } else {
            minuteString = "" + minute;
        }
        if (second < 10) {
            secondString = "0" + second;
        } else {
            secondString = "" + second;
        }
        return minuteString + ":" + secondString;
    }





    /** 发送命令，控制音乐播放。参数定义在MusicService类中 */
    private void sendBroadcastOnCommand(int command) {
        Intent intent = new Intent(MusicService.BROADCAST_MUSICSERVICE_CONTROL);
        intent.putExtra("command", command);//键名，键值
        // 根据不同命令，封装不同的数据
        switch (command) {
            case MusicService.COMMAND_PLAY:
                intent.putExtra("number", number);// 播放要返回播放的歌曲号
                break;
            case MusicService.COMMAND_SEEK_TO:
                intent.putExtra("time",time);
                break;
            case MusicService.COMMAND_PREVIOUS:
            case MusicService.COMMAND_NEXT:
            case MusicService.COMMAND_PAUSE:
            case MusicService.COMMAND_STOP:
            case MusicService.COMMAND_RESUME:
            default:
                break;
        }
        sendBroadcast(intent);
    }

    /** 内部类，用于播放器状态更新的接收广播 */
    class StatusChangedReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String musicName = intent.getStringExtra("musicName");
            String musicArtist = intent.getStringExtra("musicArtist");
            // 获取播放器状态
            status = intent.getIntExtra("status", -1);
            switch (status) {
                case MusicService.STATUS_PLAYING:
                    seekBarHandler.removeMessages(PROGRESS_INCREASE);//防止播放完成后，跳转下一首时，进度条没有归零
                    time = intent.getIntExtra("time", 0);
                    duration = intent.getIntExtra("duration", 0);
                    number = intent.getIntExtra("number", number);
                    list.setSelection(number);
                    seekBar.setProgress(time);
                    seekBar.setMax(duration);
                    seekBarHandler.sendEmptyMessageDelayed(PROGRESS_INCREASE, 1000);
                    text_Duration.setText(formatTime(duration));
                    imgBtn_PlayOrPause.setBackgroundResource(R.drawable.pause);
                    //设置textview文字，提示已经播放的歌曲
                    MainActivity.this.setTitle("正在播放:" + musicName + " - " + musicArtist);
                    MainActivity.this.textView.setText(musicArtist + " - " + musicName);
                    break;
                case MusicService.STATUS_PAUSED:
                    seekBarHandler.sendEmptyMessage(PROGRESS_PAUSE);
                    imgBtn_PlayOrPause.setBackgroundResource(R.drawable.play);
                    MainActivity.this.textView.setText(musicArtist + " - " + musicName);
                    break;
                case MusicService.STATUS_STOPPED:
                    time = 0;
                    duration = 0;
                    text_Current.setText(formatTime(time));
                    text_Duration.setText(formatTime(duration));
                    seekBarHandler.sendEmptyMessage(PROGRESS_RESET);
                    MainActivity.this.setTitle("");
                    imgBtn_PlayOrPause.setBackgroundResource(R.drawable.play);
                    MainActivity.this.textView.setText("");
                    break;
                case MusicService.STATUS_COMPLETED:
                    number = intent.getIntExtra("number", 0);
                    if(playmode == MainActivity.MODE_LIST_SEQUENCE){
                        if (number == MusicList.getMusicList().size() - 1)
                            sendBroadcastOnCommand(MusicService.STATUS_STOPPED);
                        else
                            sendBroadcastOnCommand(MusicService.COMMAND_NEXT);
                    }
                    else if(playmode == MainActivity.MODE_LIST_CYCLE)
                    {
                        if(number == MusicList.getMusicList().size() -1)
                        {
                            number = 0;
                            sendBroadcastOnCommand(MusicService.COMMAND_PLAY);
                        }
                        else
                            sendBroadcastOnCommand(MusicService.COMMAND_NEXT);
                    }
                    else if(playmode == MainActivity.MODE_SINGLE_CYCLE)
                        sendBroadcastOnCommand(MusicService.COMMAND_PLAY);

                    else if(playmode == MainActivity.MODE_RANDOM)
                    {
                        sendBroadcastOnCommand(MusicService.COMMAND_RANDOM_NEXT);
                    }

                    seekBarHandler.sendEmptyMessage(PROGRESS_RESET);
                    MainActivity.this.setTitle("");
                    imgBtn_PlayOrPause.setBackgroundResource(R.drawable.play);
                    break;
                default:
                    break;
            }
        }
    }

    protected void onDestroy() {
        if (status == MusicService.STATUS_STOPPED) {
            stopService(new Intent(this, MusicService.class));
        }
        super.onDestroy();
    }


    //设置Activity主题
    private void setTheme(String theme){
        if ("彩色".equals(theme)){
            root_Layout.setBackgroundResource(R.drawable.bg_color);
        } else if ("花朵".equals(theme)){
            root_Layout.setBackgroundResource(R.drawable.bg_digit_flower);
        } else if ("群山".equals(theme)){
            root_Layout.setBackgroundResource(R.drawable.bg_mountain);
        } else if ("小狗".equals(theme)){
            root_Layout.setBackgroundResource(R.drawable.bg_running_dog);
        } else if ("冰雪".equals(theme)){
            root_Layout.setBackgroundResource(R.drawable.bg_snow);
        } else if ("女孩".equals(theme)){
            root_Layout.setBackgroundResource(R.drawable.bg_music_girl);
        } else if ("朦胧".equals(theme)){
            root_Layout.setBackgroundResource(R.drawable.bg_blur);
        }
    }

    //创建菜单
    public boolean onCreateOptionsMenu(Menu menu){
        this.getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_theme:
                new AlertDialog.Builder(this).setTitle("选择主题").setItems(R.array.theme,
                        new android.content.DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String theme = PropertyBean.THEMES[which];
                                MainActivity.this.setTheme(theme);
                                PropertyBean property = new PropertyBean(MainActivity.this);
                                property.setAndSaveTheme(theme);
                            }
                        }).show();
                break;
            case R.id.menu_about:
                new AlertDialog.Builder(MainActivity.this).setTitle("MusicPlayer").setMessage(R.string.about2).show();
                break;
            case R.id.menu_quit:
                new AlertDialog.Builder(this).setTitle("提示").setMessage(R.string.quit).setPositiveButton("确定", new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        System.exit(0);
                    }
                }).setNegativeButton("取消", new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
                break;
            case R.id.menu_playmode:
                String[] mode = new String[]{"顺序播放","列表循环","单曲循环","随机播放"};
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("播放模式");
                builder.setSingleChoiceItems(mode,playmode,new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface arg0,int arg1){
                        playmode = arg1;
                    }
                });
                builder.setPositiveButton("确定",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface arg0,int arg1){
                        switch (playmode){
                            case 0:
                                playmode = MainActivity.MODE_LIST_SEQUENCE;
                                Toast.makeText(getApplicationContext(),R.string.sequence,Toast.LENGTH_SHORT).show();
                                break;
                            case 1:
                                playmode = MainActivity.MODE_LIST_CYCLE;
                                Toast.makeText(getApplicationContext(),R.string.listcycle,Toast.LENGTH_SHORT).show();
                                break;
                            case 2:
                                playmode = MainActivity.MODE_SINGLE_CYCLE;
                                Toast.makeText(getApplicationContext(),R.string.singlecycle,Toast.LENGTH_SHORT).show();
                                break;
                            case 3:
                                playmode = MainActivity.MODE_RANDOM;
                                Toast.makeText(getApplicationContext(),R.string.random,Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
                builder.create().show();
                break;
            case R.id.menu_sleep:
                showsleepDialog();
                break;
            case R.id.menu_lrc:
                Intent intent = new Intent(MainActivity.this,LrcActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initSeekBarHandler() {
        seekBarHandler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case PROGRESS_INCREASE:
                        if (seekBar.getProgress() < duration) {
                            //进度条前进一秒
                            seekBar.setProgress(time);
                            //seekBar.incrementProgressBy(1000);
                            seekBarHandler.sendEmptyMessageDelayed(PROGRESS_INCREASE, 1000);
                            //修改显示当前进度的文本
                            text_Current.setText(formatTime(time));
                            time += 1000;
                        }
                        break;
                    case PROGRESS_PAUSE:
                        seekBarHandler.removeMessages(PROGRESS_INCREASE);
                        break;
                    case PROGRESS_RESET:
                        //重置进度条画面
                        seekBarHandler.removeMessages(PROGRESS_INCREASE);
                        seekBar.setProgress(0);
                        text_Current.setText("00:00");
                        break;
                }
            }
        };
    }

    //按键响应
    public boolean onKeyDown(int keyCode, KeyEvent event){
        int progress;
        switch (keyCode)
        {
            case KeyEvent.KEYCODE_BACK:
                exitByDoubleClick();
                break;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                progress = seekBar_vol.getProgress();
                if(progress != 0)
                    seekBar_vol.setProgress(progress-1);
                return true;
            case KeyEvent.KEYCODE_VOLUME_UP:
                progress = seekBar_vol.getProgress();
                if(progress != 0)
                    seekBar_vol.setProgress(progress+1);
                return true;
            default:
                break;
        }
        return false;
    }

    private void exitByDoubleClick(){
        Timer timer = null;
        if(isExit ==false){
            isExit = true;
            Toast.makeText(this,"再按一次退出程序",Toast.LENGTH_SHORT).show();
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false;
                }
            },2000);
        }
        else {
            System.exit(0);
        }
    }

    //音量处理
    private void audio_Control(){
        final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        final int max_progress = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        seekBar_vol.setMax(max_progress);
        int progress = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        seekBar_vol.setProgress(progress);
        tv_vol.setText("音量："+(progress*100/max_progress)+"%");
        seekBar_vol.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            public void onStopTrackingTouch(SeekBar arg0){
            }
            public void onStartTrackingTouch(SeekBar arg0){
            }
            public void onProgressChanged(SeekBar arg0,int arg1,boolean arg2){
                tv_vol.setText("音量："+(arg1*100)/(max_progress)+"%");
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,arg1,AudioManager.FLAG_PLAY_SOUND);
            }
        });
    }

    //睡眠功能
    private void showsleepDialog() {
        final View userview = this.getLayoutInflater().inflate(R.layout.dialog,null);
        final TextView tv_minute = (TextView)userview.findViewById(R.id.dialog_tv);
        final Switch switch1 = (Switch)userview.findViewById(R.id.dialog_switch);
        final SeekBar seekBar = (SeekBar)userview.findViewById(R.id.dialog_seekbar);

        tv_minute.setText(sleepminute+"后睡眠！");
        if (sleepmode == MainActivity.ISSLEEP)switch1.setChecked(true);
        seekBar.setMax(60);
        seekBar.setProgress(sleepminute);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            public void onStopTrackingTouch(SeekBar arg0){}
            public void onStartTrackingTouch(SeekBar arg1){}
            public void onProgressChanged(SeekBar arg0,int arg1,boolean arg2){
                sleepminute = arg1;
                tv_minute.setText(sleepminute+"后睡眠！");
            }
        });
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            public void onCheckedChanged(CompoundButton arg0,boolean arg1) {
                sleepmode = arg1;
            }
        });
        final TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                System.exit(0);
            }
        };
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("选择睡眠时间（0-60）分钟");
        dialog.setView(userview);
        dialog.setNegativeButton("取消",new DialogInterface.OnClickListener(){
           public void onClick(DialogInterface arg0,int arg1){
               arg0.dismiss();
           }
        });
        dialog.setNeutralButton("重置",new DialogInterface.OnClickListener(){
           public void onClick(DialogInterface arg0,int arg1){
                if (sleepmode == MainActivity.ISSLEEP){
                    timerTask.cancel();
                    timer_sleep.cancel();
                }
                sleepmode = MainActivity.NOTSLEEP;
                sleepminute = 20;
                iv_sleep.setVisibility(View.INVISIBLE);
            }
        });
        dialog.setPositiveButton("确定",new DialogInterface.OnClickListener(){
           public void onClick(DialogInterface arg0,int arg1){
               if(sleepmode == MainActivity.ISSLEEP){
                   timer_sleep = new Timer();
                   int time = seekBar.getProgress();
                   timer_sleep.schedule(timerTask,time*60*1000);
                   iv_sleep.setVisibility(View.VISIBLE);
               }
               else {
                   timerTask.cancel();
                   if(timer_sleep != null)timer_sleep.cancel();
                   arg0.dismiss();
                   iv_sleep.setVisibility(View.INVISIBLE);
               }
           }
        });
        dialog.show();
    }
}
